module Schedule exposing (Event, Events, Person, decodeEvents)

import Json.Decode as D
import Json.Decode.Pipeline as DP
import List exposing (concatMap, map)
import Tuple exposing (second)


type alias Events =
    { url : Url, events : List Event }


decodeEvents : D.Decoder Events
decodeEvents =
    D.map2 Events
        (D.at [ "schedule", "base_url" ] D.string)
        (D.map (concatMap identity) (D.at [ "schedule", "conference", "days" ] <| D.list decodeEventsFromDays))


decodeEventsFromDays : D.Decoder (List Event)
decodeEventsFromDays =
    D.field "rooms" <|
        D.map (concatMap second) <|
            D.keyValuePairs (D.list decodeEvent)


decodeEvent : D.Decoder Event
decodeEvent =
    D.succeed Event
        |> DP.required "url" D.string
        |> DP.required "id" D.int
        |> DP.required "guid" D.string
        |> DP.required "logo" (D.maybe D.string)
        |> DP.required "date" D.string
        |> DP.required "start" D.string
        |> DP.required "duration" D.string
        |> DP.required "room" D.string
        |> DP.required "slug" D.string
        |> DP.required "title" D.string
        |> DP.required "subtitle" D.string
        |> DP.required "track" D.string
        |> DP.required "type" D.string
        |> DP.required "language" D.string
        |> DP.required "abstract" D.string
        |> DP.required "description" D.string
        |> DP.required "recording_license" D.string
        |> DP.required "do_not_record" D.bool
        |> DP.required "persons" decodePersons


decodePersons : D.Decoder (List Person)
decodePersons =
    D.list <|
        D.map2 Person
            (D.field "id" D.int)
            (D.field "public_name" D.string)


type alias Url =
    String


type alias RoomName =
    String


type alias Event =
    { url : Url
    , id : Int
    , guid : String
    , logo : Maybe Url
    , date : String
    , start : String
    , duration : String
    , room : RoomName
    , slug : String
    , title : String
    , subtitle : String
    , track : String
    , event_type : String -- TODO: It's actually called type. Fix this.
    , language : String
    , abstract : String
    , description : String
    , recording_license : String
    , do_not_record : Bool
    , persons : List Person
    }


type alias Person =
    { id : Int
    , public_name : String
    }
