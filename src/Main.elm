module Main exposing (Model(..), Msg(..), SwipeState, card, dislike, init, initial, like, main, toString, update, view)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Schedule exposing (..)
import String exposing (fromInt)


type alias SwipeState =
    { unseen : List Event
    , liked : List Event
    , disliked : List Event
    }


initial : List Event -> SwipeState
initial events =
    { unseen = events, liked = [], disliked = [] }


like : SwipeState -> SwipeState
like swst =
    let
        { unseen, liked, disliked } =
            swst
    in
    case unseen of
        f :: rest ->
            { unseen = rest, liked = f :: liked, disliked = disliked }

        [] ->
            swst


dislike : SwipeState -> SwipeState
dislike swst =
    let
        { unseen, liked, disliked } =
            swst
    in
    case unseen of
        f :: rest ->
            { unseen = rest, liked = liked, disliked = f :: disliked }

        [] ->
            swst


type Model
    = Loading
    | Error String
    | Swiping String SwipeState


type Msg
    = Loaded (Result Http.Error Events)
    | Like
    | Dislike


toString : Http.Error -> String
toString err =
    case err of
        Http.BadUrl string ->
            "Bad Url: " ++ string

        Http.Timeout ->
            "Timeout :("

        Http.NetworkError ->
            "Network Error :("

        Http.BadStatus int ->
            "Status " ++ fromInt int

        Http.BadBody string ->
            "Bad Body: " ++ string


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Loaded (Ok events) ->
            ( Swiping events.url (initial events.events), Cmd.none )

        Loaded (Err message) ->
            ( Error <| toString message, Cmd.none )

        _ ->
            case model of
                Swiping baseUrl swipeState ->
                    case msg of
                        Like ->
                            ( Swiping baseUrl <| like swipeState, Cmd.none )

                        Dislike ->
                            ( Swiping baseUrl <| dislike swipeState, Cmd.none )

                        _ ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )


view : Model -> Html Msg
view model =
    case model of
        Loading ->
            2

        Error string ->
            text string

        Swiping baseUrl { unseen, liked, disliked } ->
            case unseen of
                c :: _ ->
                    card baseUrl c

                [] ->
                    div [] [ text "No more Events in this list :(" ]


image : String -> Event -> Html Msg
image baseUrl event =
    case event.logo of
        Just logo ->
            img [ src (baseUrl ++ logo) ] []

        Nothing ->
            div [] []


card : String -> Event -> Html Msg
card baseUrl current =
    div []
        [ div []
            [ image baseUrl current
            , h1 [] [ text current.title ]
            , h2 [] [ text current.subtitle ]
            , p [] [ text current.abstract ]
            ]
        , button [ onClick Like ] [ text "Like" ]
        , button [ onClick Dislike ] [ text "Dislike" ]
        ]


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading
    , Http.request
        { method = "GET"
        , url = "http://localhost:9000/schedule.json"
        , headers = [ Http.header "accept" "application/json" ]
        , body = Http.emptyBody
        , expect = Http.expectJson Loaded decodeEvents
        , timeout = Nothing
        , tracker = Nothing
        }
    )


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , view = \model -> Browser.Document "Swiping Fahrplan" [ view model ]
        , update = update
        , subscriptions = \_ -> Sub.none
        }
