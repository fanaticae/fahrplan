with import <nixpkgs> { };
 
stdenv.mkDerivation {
  name = "env";
  buildInputs = with pkgs;[
    elmPackages.elm
    elmPackages.elm-format
  ];  
}
